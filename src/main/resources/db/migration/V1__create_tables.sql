CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       username VARCHAR(255) NOT NULL,
                       password VARCHAR(255) NOT NULL
);

CREATE UNIQUE INDEX users_username_uindex ON users (username);

CREATE TABLE roles (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255) NOT NULL
);

CREATE UNIQUE INDEX roles_name_uindex ON roles (name);

CREATE TABLE user_roles (
                            user_id INT NOT NULL,
                            role_id INT NOT NULL,
                            PRIMARY KEY (user_id, role_id),
                            FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
                            FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE
);

CREATE TABLE tests (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255) NOT NULL,
                       duration INT NOT NULL,
                       instructions TEXT,
                       total_score INT NOT NULL
);

CREATE UNIQUE INDEX tests_name_uindex ON tests (name);

CREATE TABLE questions (
                           id SERIAL PRIMARY KEY,
                           test_id INT NOT NULL,
                           question TEXT NOT NULL,
                           score INT NOT NULL,
                           FOREIGN KEY (test_id) REFERENCES tests (id) ON DELETE CASCADE
);

CREATE TABLE answers (
                         id SERIAL PRIMARY KEY,
                         question_id INT NOT NULL,
                         answer TEXT NOT NULL,
                         is_correct BOOLEAN NOT NULL,
                         FOREIGN KEY (question_id) REFERENCES questions (id) ON DELETE CASCADE
);

CREATE TABLE results (
                         id SERIAL PRIMARY KEY,
                         user_id INT NOT NULL,
                         test_id INT NOT NULL,
                         total_score INT NOT NULL,
                         FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
                         FOREIGN KEY (test_id) REFERENCES tests (id) ON DELETE CASCADE
);

CREATE TABLE result_details (
                                id SERIAL PRIMARY KEY,
                                result_id INT NOT NULL,
                                question_id INT NOT NULL,
                                answer_id INT NOT NULL,
                                FOREIGN KEY (result_id) REFERENCES results (id) ON DELETE CASCADE,
                                FOREIGN KEY (question_id) REFERENCES questions (id) ON DELETE CASCADE,
                                FOREIGN KEY (answer_id) REFERENCES answers (id) ON DELETE CASCADE
);
