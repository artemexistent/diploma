package ua.knu.servicetest.dao;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}