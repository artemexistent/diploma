package ua.knu.servicetest.dao;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "result_details")
public class ResultDetail {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "result_id", nullable = false)
  private Result result;

  @Column(nullable = false)
  private String question;

  @Column(name = "correct_answer", nullable = false)
  private String correctAnswer;

  @Column(name = "selected_answers")
  private String selectedAnswers;

  @Column(nullable = false)
  private int score;

}
