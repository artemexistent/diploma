package ua.knu.servicetest.dao;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "tests")
public class Test implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false, unique = true)
  private String name;

  @Column(nullable = false)
  private int duration;

  @Column
  private String instructions;

  @Column(name = "total_score", nullable = false)
  private int totalScore;

  @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
  private List<Question> questions = new ArrayList<>();
}
