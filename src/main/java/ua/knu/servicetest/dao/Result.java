package ua.knu.servicetest.dao;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "results")
public class Result {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @ManyToOne
  @JoinColumn(name = "test_id", nullable = false)
  private Test test;

  @Column(name = "total_score", nullable = false)
  private int totalScore;

  @OneToMany(mappedBy = "result", cascade = CascadeType.ALL)
  private List<Answer> submittedAnswers = new ArrayList<>();

  @OneToMany(mappedBy = "result", cascade = CascadeType.ALL)
  private List<ResultDetail> resultDetails = new ArrayList<>();
}