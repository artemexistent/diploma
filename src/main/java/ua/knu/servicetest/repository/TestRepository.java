package ua.knu.servicetest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.knu.servicetest.dao.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {

}
