package ua.knu.servicetest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.knu.servicetest.dao.Question;
import ua.knu.servicetest.dao.Test;
import ua.knu.servicetest.dao.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
  List<Question> findByTest(Test test);
}
