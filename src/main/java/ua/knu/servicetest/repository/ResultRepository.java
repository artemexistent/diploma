package ua.knu.servicetest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.knu.servicetest.dao.Question;
import ua.knu.servicetest.dao.Result;
import ua.knu.servicetest.dao.Test;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {
}
