package ua.knu.servicetest.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.knu.servicetest.dao.UserPrincipal;
import ua.knu.servicetest.service.AuthService;
import ua.knu.servicetest.web.model.ApiResponse;
import ua.knu.servicetest.web.model.ChangePasswordRequestDto;
import ua.knu.servicetest.web.model.JwtAuthenticationResponse;
import ua.knu.servicetest.web.model.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

  private final AuthService authService;

  @PostMapping("/register")
  public ApiResponse registerUser(@Valid @RequestBody UserDto registrationRequest) {
    return authService.registerUser(registrationRequest);
  }

  @PostMapping("/login")
  public JwtAuthenticationResponse authenticateUser(@Valid @RequestBody UserDto loginRequest) {
    String token = authService.authenticateUser(loginRequest);
    return new JwtAuthenticationResponse(token);
  }

  @PostMapping("/changepassword")
  public ApiResponse changePassword(@Valid @RequestBody ChangePasswordRequestDto changePasswordRequest, UserPrincipal principal) {
    authService.changePassword(changePasswordRequest, principal);
    return new ApiResponse(true, "Password changed successfully");
  }

  @PostMapping("/logout")
  public ApiResponse logoutUser(HttpServletRequest request, HttpServletResponse response) {
    authService.logoutUser(request, response);
    return new ApiResponse(true, "User logged out successfully");
  }
}
