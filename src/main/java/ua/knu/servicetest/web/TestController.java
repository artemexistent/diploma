package ua.knu.servicetest.web;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.knu.servicetest.service.QuestionService;
import ua.knu.servicetest.service.TestService;
import ua.knu.servicetest.web.model.QuestionDto;
import ua.knu.servicetest.web.model.ResultSummaryDto;
import ua.knu.servicetest.web.model.TestDto;
import ua.knu.servicetest.web.model.TestResultsDto;

import java.util.List;

@RestController
@RequestMapping("/api/tests")
@RequiredArgsConstructor
public class TestController {

  private final TestService testService;
  private final QuestionService questionService;

  @GetMapping("/{id}")
  public TestDto getTestById(@PathVariable Long id) {
      return testService.getTestById(id);
  }

  @GetMapping
  public List<TestDto> getAllTests() {
      return testService.getAllTests();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public TestDto createTest(@RequestBody TestDto testDto) {
      return testService.createTest(testDto);
  }

  @PutMapping("/{id}")
  public TestDto updateTest(@PathVariable Long id, @RequestBody TestDto testDto) {
      return testService.updateTest(id, testDto);
  }

  @DeleteMapping("/{id}")
  public void deleteTest(@PathVariable Long id) {
    testService.deleteTest(id);
  }

  @PostMapping("/{id}/submit")
  public ResultSummaryDto submitTest(@PathVariable Long id, @RequestBody TestResultsDto testResultsDto) {
      return testService.submitTestResults(id, testResultsDto);
  }

  @PostMapping("/{testId}/questions")
  @ResponseStatus(HttpStatus.CREATED)
  public TestDto createQuestion(@PathVariable Long testId, @RequestBody QuestionDto questionDto) {
    return testService.addQuestionToTest(testId, questionDto);
  }

  @PutMapping("/{testId}/questions/{questionId}")
  public QuestionDto updateQuestion(@PathVariable Long testId, @PathVariable Long questionId,
                                                    @RequestBody QuestionDto questionDto) {
    TestDto testDto = testService.getTestById(testId);
    return questionService.updateQuestion(testDto, questionId, questionDto);
  }

  @GetMapping("/{testId}/questions/{questionId}")
  public TestDto getQuestion(@PathVariable Long testId, @PathVariable Long questionId) {
    return testService.removeQuestionFromTest(testId, questionId);
  }
}
