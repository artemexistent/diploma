package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Data
@Value
@RequiredArgsConstructor
public class TestDto {
  Long id;
  String name;
  int duration;
  String instructions;
  int totalScore;
  List<QuestionDto> questions;
}
