package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Data
@Value
@RequiredArgsConstructor
public class QuestionDto {
  Long id;
  String question;
  int score;
  List<AnswerDto> answers;
}
