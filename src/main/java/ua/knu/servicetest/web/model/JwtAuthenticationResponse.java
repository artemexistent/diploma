package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.Value;

@Data
@Value
public class JwtAuthenticationResponse {
  String accessToken;
  String tokenType;

  public JwtAuthenticationResponse(String accessToken) {
    this.accessToken = accessToken;
    this.tokenType = "Bearer";
  }
}
