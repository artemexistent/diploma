package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;
import java.util.Set;

@Data
@Value
@RequiredArgsConstructor
public class UserDto {
  String username;
  String password;
  Set<String> roles;
}
