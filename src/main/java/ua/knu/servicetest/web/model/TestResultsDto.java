package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Data
@Value
@RequiredArgsConstructor
public class TestResultsDto {
  private List<AnswersDto> submittedAnswers;
}
