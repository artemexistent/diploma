package ua.knu.servicetest.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Data
@Value
@RequiredArgsConstructor
public class LoginRequestDto {
  String username;
  String password;
}
