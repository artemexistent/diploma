package ua.knu.servicetest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ua.knu.servicetest.dao.Role;
import ua.knu.servicetest.dao.RoleName;
import ua.knu.servicetest.dao.User;
import ua.knu.servicetest.dao.UserPrincipal;
import ua.knu.servicetest.exception.BadRequestException;
import ua.knu.servicetest.exception.ResourceNotFoundException;
import ua.knu.servicetest.exception.UnauthorizedException;
import ua.knu.servicetest.repository.RoleRepository;
import ua.knu.servicetest.repository.UserRepository;
import ua.knu.servicetest.util.CookieUtil;
import ua.knu.servicetest.util.TokenExtractor;
import ua.knu.servicetest.web.model.ApiResponse;
import ua.knu.servicetest.web.model.ChangePasswordRequestDto;
import ua.knu.servicetest.web.model.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthService {

  private final PasswordEncoder passwordEncoder;
  private final JwtTokenProvider tokenProvider;
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final AuthenticationManager authenticationManager;

  public ApiResponse registerUser(UserDto registrationRequestDto) {
    String username = registrationRequestDto.getUsername();
    if (userRepository.existsByUsername(username)) {
      return new ApiResponse(false, "Username is already taken!");
    }

    User user = new User();
    user.setUsername(username);
    user.setPassword(passwordEncoder.encode(registrationRequestDto.getPassword()));

    Set<String> strRoles = registrationRequestDto.getRoles();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null || strRoles.isEmpty()) {
      Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Default user role not found!"));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Admin role not found!"));
            roles.add(adminRole);
            break;
          default:
            Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Default user role not found!"));
            roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);

    return new ApiResponse(true, "User registered successfully");
  }

  public String authenticateUser(UserDto loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            loginRequest.getUsername(),
            loginRequest.getPassword()
        )
    );

    SecurityContextHolder.getContext().setAuthentication(authentication);
    return tokenProvider.generateToken(authentication);
  }

  public void changePassword(ChangePasswordRequestDto changePasswordRequest, UserPrincipal userPrincipal) {
    User user = userRepository.findById(userPrincipal.getId())
        .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));

    if (!passwordEncoder.matches(changePasswordRequest.getOldPassword(), user.getPassword())) {
      throw new BadRequestException("Invalid old password");
    }

    user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
    userRepository.save(user);
  }

  public User getCurrentUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null && authentication.isAuthenticated()) {
      UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
      return userRepository.findById(userPrincipal.getId())
          .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }
    throw new UnauthorizedException("No authenticated user found");
  }

  public void logoutUser(HttpServletRequest request, HttpServletResponse response) {
    String token = TokenExtractor.extractToken(request);

    if (token != null && tokenProvider.validateToken(token)) {
      CookieUtil.deleteCookie(response, "jwtToken");
    } else {
      throw new UnauthorizedException("Invalid or expired token.");
    }
  }
}
