package ua.knu.servicetest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.knu.servicetest.dao.Answer;
import ua.knu.servicetest.dao.Question;
import ua.knu.servicetest.dao.Result;
import ua.knu.servicetest.dao.Test;
import ua.knu.servicetest.dao.User;
import ua.knu.servicetest.exception.BadRequestException;
import ua.knu.servicetest.exception.ResourceNotFoundException;
import ua.knu.servicetest.repository.QuestionRepository;
import ua.knu.servicetest.repository.ResultRepository;
import ua.knu.servicetest.repository.TestRepository;
import ua.knu.servicetest.web.model.AnswersDto;
import ua.knu.servicetest.web.model.QuestionDto;
import ua.knu.servicetest.web.model.ResultSummaryDto;
import ua.knu.servicetest.web.model.TestDto;
import ua.knu.servicetest.web.model.TestResultsDto;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TestService {

  private final TestRepository testRepository;
  private final QuestionService questionService;
  private final ResultRepository resultRepository;
  private final AuthService authService;

  public TestDto createTest(TestDto testDto) {
    Test test = new Test();
    test.setName(testDto.getName());
    test.setDuration(testDto.getDuration());
    test.setInstructions(testDto.getInstructions());
    test.setTotalScore(testDto.getTotalScore());

    Test savedTest = testRepository.save(test);

    return mapTestToDto(savedTest);
  }

  public TestDto getTestById(Long id) {
    Test test = testRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", id));

    return mapTestToDto(test);
  }

  public List<TestDto> getAllTests() {
    List<Test> tests = testRepository.findAll();

    return tests.stream()
        .map(this::mapTestToDto)
        .toList();
  }

  public TestDto updateTest(Long id, TestDto testDto) {
    Test test = testRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", id));

    test.setName(testDto.getName());
    test.setDuration(testDto.getDuration());
    test.setInstructions(testDto.getInstructions());
    test.setTotalScore(testDto.getTotalScore());

    Test updatedTest = testRepository.save(test);

    return mapTestToDto(updatedTest);
  }

  public void deleteTest(Long id) {
    Test test = testRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", id));

    testRepository.delete(test);
  }

  public TestDto addQuestionToTest(Long testId, QuestionDto questionDto) {
    Test test = testRepository.findById(testId)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", testId));

    Question savedQuestion = questionService.createQuestion(questionDto, test);
    test.getQuestions().add(savedQuestion);

    Test updatedTest = testRepository.save(test);

    return mapTestToDto(updatedTest);
  }

  public TestDto removeQuestionFromTest(Long testId, Long questionId) {
    Test test = testRepository.findById(testId)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", testId));

    test.getQuestions().removeIf(question -> question.getId().equals(questionId));

    Test updatedTest = testRepository.save(test);

    return mapTestToDto(updatedTest);
  }

  public ResultSummaryDto submitTestResults(Long testId, TestResultsDto testResultsDto) {
    Test test = testRepository.findById(testId)
        .orElseThrow(() -> new ResourceNotFoundException("Test", "id", testId));

    User user = authService.getCurrentUser();

    Result result = new Result();
    result.setUser(user);
    result.setTest(test);
    result.setTotalScore(0);
    result.setSubmittedAnswers(new ArrayList<>());
    for (int i = 0; i < test.getQuestions().size(); i++) {
      Question question = test.getQuestions().get(i);
      AnswersDto answersDto = testResultsDto.getSubmittedAnswers().get(i);

      if (!questionService.isAnswerCorrect(question, answersDto)) {
        throw new BadRequestException("Answer is not correct for question with id: " + question.getId());
      }

      result.setTotalScore(result.getTotalScore() + question.getScore());

      final var answers = new ArrayList<Answer>();

      answersDto.getAnswers().forEach(answerDto -> {
        Answer answer = new Answer();
        answer.setQuestion(question);
        answer.setAnswer(answerDto);
        answer.setCorrect(true);
        answers.add(answer);
      });
      result.getSubmittedAnswers().addAll(answers);
    }

    result = resultRepository.save(result);

    return new ResultSummaryDto(result.getId(), test.getName(), result.getTotalScore(), result.getSubmittedAnswers().size());
  }

  private TestDto mapTestToDto(Test test) {
    return new TestDto(
        test.getId(),
        test.getName(),
        test.getDuration(),
        test.getInstructions(),
        test.getTotalScore(),
        List.of()
    );
  }
}
