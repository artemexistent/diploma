package ua.knu.servicetest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.knu.servicetest.dao.Answer;
import ua.knu.servicetest.dao.Question;
import ua.knu.servicetest.dao.Test;
import ua.knu.servicetest.exception.BadRequestException;
import ua.knu.servicetest.exception.ResourceNotFoundException;
import ua.knu.servicetest.repository.QuestionRepository;
import ua.knu.servicetest.web.model.AnswerDto;
import ua.knu.servicetest.web.model.AnswersDto;
import ua.knu.servicetest.web.model.QuestionDto;
import ua.knu.servicetest.web.model.TestDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionService {

  private final QuestionRepository questionRepository;

  public Question createQuestion(QuestionDto questionDto, Test test) {
    Question question = new Question();
    question.setQuestion(questionDto.getQuestion());
    question.setScore(questionDto.getScore());
    question.setTest(test);
    return questionRepository.save(question);
  }

  public Question getQuestionById(Long id) {
    return questionRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Question", "id", id));
  }

  public List<Question> getAllQuestionsByTest(Test test) {
    return questionRepository.findByTest(test);
  }

  public QuestionDto updateQuestion(TestDto testDto, Long questionId, QuestionDto questionDto) {
    Question existingQuestion = questionRepository.findById(questionId)
        .orElseThrow(() -> new ResourceNotFoundException("Question", "id", questionId));

    if (!testDto.getId().equals(existingQuestion.getTest().getId())) {
      throw new BadRequestException("Question with id: " + questionId + " does not belong to the test with id: " + testDto.getId());
    }

    existingQuestion.setQuestion(questionDto.getQuestion());
    existingQuestion.setScore(questionDto.getScore());

    List<Answer> existingAnswers = existingQuestion.getAnswers();
    List<AnswerDto> answerDtos = questionDto.getAnswers();

    existingAnswers.clear();

    for (AnswerDto answerDto : answerDtos) {
      Answer answer = new Answer();
      answer.setAnswer(answerDto.getAnswer());
      answer.setCorrect(answerDto.isCorrect());
      answer.setQuestion(existingQuestion);
      existingAnswers.add(answer);
    }

    Question updatedQuestion = questionRepository.save(existingQuestion);
    return toQuestionDto(updatedQuestion);
  }

  private QuestionDto toQuestionDto(Question question) {

    List<AnswerDto> answerDtos = new ArrayList<>();
    for (Answer answer : question.getAnswers()) {
      answerDtos.add(toAnswerDto(answer));
    }

    return new QuestionDto(question.getId(), question.getQuestion(), question.getScore(), answerDtos);
  }

  private AnswerDto toAnswerDto(Answer answer) {
    return new AnswerDto(answer.getId(), answer.getAnswer(), answer.isCorrect());
  }

  public boolean isAnswerCorrect(Question question, AnswersDto answerDto) {
    List<Answer> correctAnswers = question.getAnswers().stream()
        .filter(Answer::isCorrect)
        .toList();

    List<String> selectedAnswers = answerDto.getAnswers();

    if (correctAnswers.size() != selectedAnswers.size()) {
      return false;
    }

    List<String> correctAnswerIds = correctAnswers.stream()
        .map(Answer::getAnswer)
        .toList();

    return new HashSet<>(correctAnswerIds).containsAll(selectedAnswers);
  }

  public void deleteQuestion(Question question) {
    questionRepository.delete(question);
  }
}